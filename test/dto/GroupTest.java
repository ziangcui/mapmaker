package dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GroupTest {

    private Group group;

    @BeforeEach
    void setUp() {
        group=new Group();
    }

    @Test
    void setAndgetName() {
        group.setName("one");
        assertEquals("one",group.getName());
    }


    @Test
    void setAndGetId() {
        group.setId("1");
        assertEquals("1",group.getId());
    }
}