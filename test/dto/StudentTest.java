package dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    private Student student;

    @BeforeEach
    void setUp() {
        student=new Student();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void setAndGetId() {
        student.setId("001");
        assertEquals ("001",student.getId());
    }


    @Test
    void setAndGetFirstName() {
        student.setFirstName("Shahriar");
        assertEquals("Shahriar",student.getFirstName());
    }

    @Test
    void setAndGetLastName() {
        student.setFirstName("Emami");
        assertEquals("Emami",student.getFirstName());
    }

}