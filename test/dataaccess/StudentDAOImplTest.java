package dataaccess;

import dto.Student;
import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StudentDAOImplTest {

    TomcatJNDI tomcatJNDI;
    private StudentDAOImpl studentDAO;

    @BeforeEach
    void setUp(){
        tomcatJNDI = new TomcatJNDI();
        String webFolder = "web";
        if (!Files.exists(Paths.get(webFolder))) {
            webFolder = "WebContent";
        }
        tomcatJNDI.processContextXml(Paths.get(webFolder + "/META-INF/context.xml").toFile());
        tomcatJNDI.processWebXml(Paths.get(webFolder + "/WEB-INF/web.xml").toFile());
        tomcatJNDI.start();

        studentDAO = new StudentDAOImpl();
    }

    @Test
    void getAll() {
        int n = studentDAO.getAll().size();
        studentDAO.add(new Student("001", "Shahriar", "Emami"));
        assertEquals(n + 1, studentDAO.getAll().size());
    }
}