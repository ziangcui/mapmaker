package business;

import hthurow.tomcatjndi.TomcatJNDI;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class StudentLogicTest {

    private static TomcatJNDI tomcatJNDI;
    private StudentLogic studentLogic;

    @BeforeAll
    static void setUpAll() {
        tomcatJNDI = new TomcatJNDI();
        String webFolder = "web";
        if (!Files.exists(Paths.get(webFolder))) {
            webFolder = "WebContent";
        }
        tomcatJNDI.processContextXml(Paths.get(webFolder + "/META-INF/context.xml").toFile());
        tomcatJNDI.processWebXml(Paths.get(webFolder + "/WEB-INF/web.xml").toFile());
        tomcatJNDI.start();
    }

    @BeforeEach
    void setUp() {
        studentLogic = new StudentLogic();
    }

    @Test
    void name() {
    }

    @AfterAll
    public static void tearDownAll() {
        tomcatJNDI.tearDown();
    }

    @Test
    void getAllStudents() {
        assertEquals(3, studentLogic.getAllStudents().size());
    }
}