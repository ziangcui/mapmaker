<%@ page import="business.StudentLogic" %>
<%@ page import="dto.Student" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: cuiziang
  Date: 2018-12-07
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>


<main role="main" class="container">

    <div class="row">
        <div class="col-8">
            <h2>Student Table</h2>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <!-- https://www.w3schools.com/css/css_table.asp -->
            <form action="studentform" method="post">
                <div class="col-4">
                    <table class="table">
                        <tr>Search Student by Student Number</tr>
                        <tr>
                            <td><input id="studentId" type="text" name="searchText"/></td>
                            <td><input type="button" name="search" value="Search" onclick="getByID()"/></td>
                        </tr>
                    </table>

                </div>

                <div class="alert alert-success fade show" style="display: none" role="alert" id="resultcontainer">

                    <h4 class="alert-heading">Cheepono</h4>
                    <p id="studentResult">Aww yeah, you successfully read this important alert message. This example
                        text is going to run a bit longer so that you can see how spacing within an alert works with
                        this kind of content.</p>
                </div>

                <table class="table table-hover table-bordered" align="center">
                    <thead>
                    <tr>
                        <th class="delete"><input type="submit" name="deleteStudentSubmit" value="Delete"/></th>
                        <th>Edit</th>
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        StudentLogic logic = new StudentLogic();
                        List<Student> students = logic.getAllStudents();
                        long counter = 0;
                        for (Student student : students) {
                    %>
                    <tr>
                        <td>
                            <input type="checkbox" name="deleteMark" value="<%=student.getId()%>"/>
                        </td>
                        <td class="edit" id="<%=counter++%>"><input class="update" type="button" name="edit"
                                                                    value="Edit"/>
                        </td>
                        <td class="code" id="<%=counter++%>"><%=student.getId()%>
                        </td>
                        <td class="code" id="<%=counter++%>"><%=student.getFirstName()%>
                        </td>
                        <td class="name" id="<%=counter++%>"><%=student.getLastName()%>
                        </td>
                    </tr>
                    </tbody>
                    <%
                        }
                    %>
                    <thead>
                    <tr>
                        <th><input class="delete" type="submit" name="deleteStudentSubmit" value="Delete"/></th>
                        <th>Edit</th>
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                    </thead>
                </table>
            </form>
            <a class="btn btn-primary" href="addstudent" role="button">Add Student</a>
            <div style="text-align: center;">
                <pre><%=toStringMap(request.getParameterMap())%></pre>
            </div>
            <%!
                private String toStringMap(Map<String, String[]> m) {
                    StringBuilder builder = new StringBuilder();
                    for (String k : m.keySet()) {
                        builder.append("Key=").append(k)
                                .append(", ")
                                .append("Value/s=").append(Arrays.toString(m.get(k)))
                                .append(System.lineSeparator());
                    }
                    return builder.toString();
                }
            %>
        </div>
    </div>

</main>
<!-- /.container -->


<script>
    function getByID() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("studentResult").innerHTML = this.responseText;
                document.getElementById("resultcontainer").style.display = "";
            }
        };
        xhttp.open("GET", 'studentform?search=""&student=' + document.getElementById("studentId").value, true);
        xhttp.send();
    }
</script>

<%@include file="footer.jsp" %>
