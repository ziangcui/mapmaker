<%@ page import="business.GroupLogic" %>
<%@ page import="dto.Group" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%--
  Created by IntelliJ IDEA.
  User: cuiziang
  Date: 2018-12-07
  Time: 13:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>


<main role="main" class="container">

    <div class="row">
        <div class="col mt-8">
            <h2>Group Table</h2>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <!-- https://www.w3schools.com/css/css_table.asp -->
            <form action="groupform" method="post">
                <div class="col-4">
                    <table class="table">
                        <tr>Search Group by Group ID</tr>
                        <tr>
                            <td><input id="groupId" type="text" name="searchText"/></td>
                            <td><input type="button" name="search" value="Search" onclick="getByID()"/></td>
                        </tr>
                    </table>

                </div>

                <div class="alert alert-success fade show" style="display: none" role="alert" id="resultcontainer">
                    <h4 class="alert-heading">Cheepono</h4>
                    <p id="groupResult"></p>
                </div>

                <table class="table table-hover table-bordered" align="center">
                    <thead>
                    <tr>
                        <th><input type="submit" name="deleteGroupSubmit" value="Delete"/></th>
                        <th>Edit</th>
                        <th>Group ID</th>
                        <th>Group Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        GroupLogic logic = new GroupLogic();
                        List<Group> groups = logic.getAllGroups();
                        long counter = 0;
                        for (Group group : groups) {
                    %>
                    <tr>
                        <td class="delete">
                            <input type="checkbox" name="deleteMark" value="<%=group.getId()%>"/>
                        </td>
                        <td class="edit" id="<%=counter++%>"><input class="update" type="button" name="edit"
                                                                    value="Edit"/>
                        </td>
                        <td class="code" id="<%=counter++%>"><%=group.getId()%>
                        </td>
                        <td class="code" id="<%=counter++%>"><%=group.getName()%>
                        </td>
                    </tr>
                    </tbody>
                    <%
                        }
                    %>
                    <thead>
                    <tr>
                        <th><input type="submit" name="deleteGroupSubmit" value="Delete"/></th>
                        <th>Edit</th>
                        <th>Group ID</th>
                        <th>Group Name</th>
                    </tr>
                    </thead>
                </table>
            </form>
            <a class="btn btn-primary" href="addgroup" role="button">Add Group</a>
            <div style="text-align: center;">
                <pre><%=toStringMap(request.getParameterMap())%></pre>
            </div>
            <%!
                private String toStringMap(Map<String, String[]> m) {
                    StringBuilder builder = new StringBuilder();
                    for (String k : m.keySet()) {
                        builder.append("Key=").append(k)
                                .append(", ")
                                .append("Value/s=").append(Arrays.toString(m.get(k)))
                                .append(System.lineSeparator());
                    }
                    return builder.toString();
                }
            %>
        </div>
    </div>
</main>
<!-- /.container -->

<script>
    function getByID() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("groupResult").innerHTML = this.responseText;
                document.getElementById("resultcontainer").style.display = "";
            }
        };
        xhttp.open("GET", 'groupform?search=""&group=' + document.getElementById("groupId").value, true);
        xhttp.send();
    }
</script>

<%@include file="footer.jsp" %>
