<%@ page import="business.FileDetailLogic" %>
<%@ page import="business.FileLogic" %>
<%@ page import="business.GroupLogic" %>
<%@ page import="dto.File" %>
<%@ page import="dto.Group" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: cuiziang
  Date: 2018-12-09
  Time: 00:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>

<%

    GroupLogic groupLogic = new GroupLogic();
    List<Group> groups = groupLogic.getAllGroups();

    FileLogic logic = new FileLogic();
    List<File> files = logic.getAllFiles();

    FileDetailLogic fileDetailLogic = new FileDetailLogic();
%>

<main role="main" class="container">

    <div class="row">
        <div class="col mt-6">
            <h2>Assign Student to Group</h2>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <form action="assignfileform" method="post">

                <div class="alert alert-primary center-pill" role="alert">
                    <div style="text-align: center;">ASSIGN</div>
                </div>


                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="studentSelector">File</label>
                    </div>
                    <select class="custom-select" id="studentSelector" name='file'>
                        <option selected>Choose...</option>
                        <%
                            for (File file : files) {
                        %>
                        <option value="<%=file.getId()%>"><%=file.getId()%>
                            : <%=file.getId()%> <%=fileDetailLogic.getById(file.getId()).getName()%>
                        </option>
                        <%
                            }
                        %>
                    </select>
                </div>

                <div class="alert alert-primary center-pill" role="alert">
                    <div style="text-align: center;">TO</div>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="groupSelector">Group</label>
                    </div>
                    <select class="custom-select" id="groupSelector">
                        <option selected>Choose...</option>
                        <%
                            for (Group group : groups) {
                        %>
                        <option value="<%=group.getId()%>"><%=group.getId()%>: <%=group.getName()%>
                        </option>
                        <%
                            }
                        %>
                    </select>
                </div>


                <button type="submit" name="matchsgSubmit" class="btn btn-primary">Submit</button>

            </form>
        </div>
    </div>
</main>
<!-- /.container -->


<script>
    function getByID() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("studentResult").innerHTML = this.responseText;
                document.getElementById("resultcontainer").style.display = "";
            }
        };
        xhttp.open("GET", 'studentform?search=""&student=' + document.getElementById("studentId").value, true);
        xhttp.send();
    }
</script>

<%@include file="footer.jsp" %>
