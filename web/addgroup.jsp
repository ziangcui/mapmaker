<%--
  Created by IntelliJ IDEA.
  User: cuiziang
  Date: 2018-12-08
  Time: 16:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>

<%
    String errorMessage = request.getParameter("errorMessage");
%>
<main role="main" class="container">

    <div class="row">
        <div class="col mt-6">
            <h2>Add Group</h2>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div style="display: inline-block; text-align: left;">
                <form action="groupform" method="post">
                    <div class="form-group">
                        <label for="groupName">Group Name</label>
                        <input type="text" class="form-control" id="groupName" name="groupName">
                    </div>
                    <button type="submit" name="addGroupSubmit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>


</main>
<!-- /.container -->

<%@include file="footer.jsp" %>
