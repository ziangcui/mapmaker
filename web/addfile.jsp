<%--
Created by IntelliJ IDEA.
User: cuiziang
Date: 2018-12-07
Time: 13:43
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>

<%
    String errorMessage = request.getParameter("errorMessage");
%>
<main role="main" class="container">

    <div class="row">
        <div class="col mt-6">
            <h2>File Table</h2>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div style="display: inline-block; text-align: left;">
                <form action="fileform" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <%--Name:<br>--%>
                        <%--<input type="text" name="%s" value=""><br><%=FileDetail.COL_NAME%>--%>
                        <%--Type:<br>--%>
                        <%--<input type="text" name="%s" value=""><br><br><%=FileDetail.COL_TYPE%>--%>
                        <input type="file" name="file"/><br>
                        <input type="submit" name="view" value="Upload and View">
                        <input type="submit" name="upload" value="Upload"><br>
                    </div>
                </form>
                <%
                    if (errorMessage != null && !errorMessage.isEmpty()) {
                %>
                <p color=red>
                        <span style="color: red; ">
                        <%= errorMessage%>
                        </span>
                </p>
                Submitted keys and values:
                ${toStringMap}
                <%

                    }
                %>

            </div>
        </div>
    </div>


</main>
<!-- /.container -->

<%@include file="footer.jsp" %>