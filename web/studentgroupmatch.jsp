<%@ page import="business.GroupLogic" %>
<%@ page import="business.StudentGroupMatchLogic" %>
<%@ page import="business.StudentLogic" %>
<%@ page import="dto.StudentGroupMatch" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: cuiziang
  Date: 2018-12-09
  Time: 02:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp" %>


<main role="main" class="container">

    <div class="row">
        <div class="col-8">
            <h2>Student and Group Match Table</h2>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <!-- https://www.w3schools.com/css/css_table.asp -->
            <form action="studentgroupmatchform" method="post">

                <table class="table table-hover table-bordered" align="center">
                    <thead>
                    <tr>
                        <th class="delete"><input type="submit" name="deleteStudentGroupMatchSubmit" value="Delete"/>
                        </th>
                        <th>Edit</th>
                        <th>Matching Date</th>
                        <th>Student Name</th>
                        <th>Group Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        StudentGroupMatchLogic logic = new StudentGroupMatchLogic();
                        List<StudentGroupMatch> studentGroupMatches = logic.getAllMatchs();

                        StudentLogic studentLogic = new StudentLogic();
                        GroupLogic groupLogic = new GroupLogic();

                        long counter = 0;
                        for (StudentGroupMatch studentGroupMatch : studentGroupMatches) {
                    %>
                    <tr>
                        <td>
                            <input type="checkbox" name="deleteMark" value="<%=studentGroupMatch.getStudentId()%>"/>
                        </td>
                        <td class="edit" id="<%=counter++%>"><input class="update" type="button" name="edit"
                                                                    value="Edit"/>
                        </td>
                        <td class="code"
                            id="<%=counter++%>"><%=studentGroupMatch.getDate()%>
                        </td>
                        <td class="code"
                            id="<%=counter++%>"><%=studentLogic.getById(studentGroupMatch.getStudentId()).getLastName()%> <%=studentLogic.getById(studentGroupMatch.getStudentId()).getFirstName()%>
                        </td>
                        <td class="code"
                            id="<%=counter++%>"><%=groupLogic.getById(studentGroupMatch.getGroupId()).getName()%>
                        </td>
                    </tr>
                    </tbody>
                    <%
                        }
                    %>
                    <thead>
                    <tr>
                        <th><input class="delete" type="submit" name="deleteStudentSubmit" value="Delete"/></th>
                        <th>Edit</th>
                        <th>Student ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                    </thead>
                </table>
            </form>
            <a class="btn btn-primary" href="addstudentgroupmatch" role="button">Assign Student to Group</a>
            <div style="text-align: center;">

            </div>
        </div>
    </div>
</main>
<!-- /.container -->


<%@include file="footer.jsp" %>

