package view;

import business.GroupLogic;
import business.StudentGroupMatchLogic;
import business.StudentLogic;
import dto.Group;
import dto.Student;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StudentGroupMatchForm extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StudentGroupMatchLogic studentGroupMatchLogic = new StudentGroupMatchLogic();
        GroupLogic groupLogic = new GroupLogic();
        StudentLogic studentLogic = new StudentLogic();
        if (request.getParameter("matchsgSubmit") != null) {
            Student student = studentLogic.getById(request.getParameter("student"));
            Group group = groupLogic.getById(request.getParameter("group"));
            studentGroupMatchLogic.matchStudentGroup(student, group);
            response.sendRedirect("studentgroupmatch");
        } else if (request.getParameter("deleteStudentSubmit") != null) {
            studentGroupMatchLogic.deleteStudentGroupMatch(request.getParameterValues("deleteMark"));
            response.sendRedirect("studentgroupmatch");
        } else {
            response.sendRedirect("studentgroupmatch");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
