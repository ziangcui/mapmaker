package view;

import business.StudentLogic;
import dto.Student;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class StudentForm extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StudentLogic studentLogic = new StudentLogic();

        if (req.getParameter("addStudentSubmit") != null) {
            String id = req.getParameter("id");
            String firstName = req.getParameter("firstName");
            String lastName = req.getParameter("lastName");
            Student student = new Student(id, firstName, lastName);
            studentLogic.add(student);
            resp.sendRedirect("student");
        } else if (req.getParameter("deleteStudentSubmit") != null) {
            studentLogic.deleteStudents(req.getParameterValues("deleteMark"));
            resp.sendRedirect("student");
        } else {
            resp.sendRedirect("student");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        StudentLogic studentLogic = new StudentLogic();

        if (req.getParameter("search") != null) {
            Student student = studentLogic.getById(req.getParameter("student"));
            PrintWriter out = resp.getWriter();
            resp.setContentType("text/html;charset=UTF-8");
            if (student != null) {
                out.print("The Student called " + student.getFirstName() + " " + student.getLastName() + ", his/her student number is " + student.getId() + ".");
            } else {
                out.print("Student not found");
            }
            out.flush();
        }
    }
}
