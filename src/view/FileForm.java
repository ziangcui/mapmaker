package view;

import business.FileDetailLogic;
import business.FileLogic;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map;

/**
 * @author Shawn
 */
@MultipartConfig
public class FileForm extends HttpServlet {

    private String errorMessage = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (errorMessage != null && !errorMessage.isEmpty()) {
                out.println("<p color=red>");
                out.println("<font color=red size=4px>");
                out.println(errorMessage);
                out.println("</font>");
                out.println("</p>");
            }
            out.println("<pre>");
            out.println("Submitted keys and values:");
            String toStringMap = toStringMap(request.getParameterMap());
            request.setAttribute("errorMessage", errorMessage);
            request.setAttribute("message", toStringMap);
        }
    }

    private String toStringMap(Map<String, String[]> values) {
        StringBuilder builder = new StringBuilder();
        values.forEach((k, v) -> builder.append("Key=").append(k)
                .append(", ")
                .append("Value/s=").append(Arrays.toString(v))
                .append(System.lineSeparator()));
        return builder.toString();
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        FileLogic fileLogic = new FileLogic();
        FileDetailLogic fileDetailLogic = new FileDetailLogic();
        if (request.getParameter("upload") != null) {
            String generatedKey = (String.valueOf(addFile(request)));
            addFileDetail(request, generatedKey);
            processRequest(request, response);
        } else if (request.getParameter("view") != null) {
            String generatedKey = (String.valueOf(addFile(request)));
            addFileDetail(request, generatedKey);
            response.sendRedirect("file");
        } else if (request.getParameter("deleteFileSubmit") != null) {
            fileDetailLogic.deleteFileDetails(request.getParameterValues("deleteMark"));
            fileLogic.deleteFiles(request.getParameterValues("deleteMark"));
            response.sendRedirect("file");
        } else {
            processRequest(request, response);
        }
    }

    private int addFile(HttpServletRequest request) throws IOException, ServletException {
        FileLogic fLogic = new FileLogic();
        return fLogic.addFile(request.getPart("file").getInputStream());
    }

    private void addFileDetail(HttpServletRequest request, String key) throws IOException, ServletException {
        FileDetailLogic fileDetailLogic = new FileDetailLogic();

        fileDetailLogic.addFileDetail(request.getPart("file"), key);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
