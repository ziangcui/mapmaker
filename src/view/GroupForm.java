package view;

import business.GroupLogic;
import dto.Group;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class GroupForm extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GroupLogic groupLogic = new GroupLogic();
        String d = req.getParameter("deleteGroupSubmit");
        if (req.getParameter("addGroupSubmit") != null) {
            String name = req.getParameter("groupName");
            Group group = new Group(name);
            groupLogic.addGroup(group);
            resp.sendRedirect("group");
        } else if (req.getParameter("deleteGroupSubmit") != null) {
            groupLogic.deleteGroup(req.getParameterValues("deleteMark"));
            resp.sendRedirect("group");
        } else {
            resp.sendRedirect("group");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        GroupLogic groupLogic = new GroupLogic();

        if (req.getParameter("search") != null) {
            Group group = groupLogic.getById(req.getParameter("group"));
            PrintWriter out = resp.getWriter();
            resp.setContentType("text/html;charset=UTF-8");
            if (group != null) {
                out.print("The Group called " + group.getName() + ", and group number is " + group.getId() + ".");
            } else {
                out.print("Group not found");
            }
            out.flush();
        }
    }
}
