package dto;

public class FileDetail {

    public static final String COL_NAME = "name";
    public static final String COL_TYPE = "type";
    public static final String COL_DATE = "date";
    public static final String COL_SIZE = "size";

    private String name;
    private String type;
    private String date;
    private String size;

    public FileDetail() {
    }

    public FileDetail(String name, String type, String date, String size) {
        this.name = name;
        this.type = type;
        this.date = date;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
