package dto;

public class File {
    public static final String COL_ID = "id";
    public static final String COL_FILE = "file";

    private String id;
    private String file;

    public File() {
    }

    public File(String file) {
        this.file = file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
