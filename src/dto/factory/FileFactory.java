/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.factory;


import dto.File;
import dto.builder.FileBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Min Jia
 */
public class FileFactory implements Factory<File> {
    public File createFromResultSet(ResultSet rs) throws SQLException {
        if (rs == null || !rs.next()) {
            return null;
        }
        FileBuilder builder = new FileBuilder();
        builder.setId(rs.getString(File.COL_ID));
        builder.setFile(rs.getBlob(File.COL_FILE).toString());
        return builder.getFile();
    }

    @Override
    public List<File> createListFromResultSet(ResultSet rs) throws SQLException {
        if (rs == null || !rs.next()) {
            return null;
        }
        FileBuilder builder;
        List<File> files;
        files = new ArrayList<>(100);
        while (rs.next()) {
            builder = new FileBuilder();
            builder.setId(rs.getString(File.COL_ID))
                    .setFile(rs.getString(File.COL_FILE));
            files.add(builder.get());
        }
        return files;
    }

    public File createFromMap(Map<String, String[]> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        FileBuilder builder = new FileBuilder();
        builder.setId(map.get(File.COL_ID)[0])
                .setFile(map.get(File.COL_FILE)[0]);
        return builder.getFile();
    }
}
