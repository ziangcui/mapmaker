/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.factory;

import dto.FileDetail;
import dto.builder.FileDetailBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Min Jia
 */
public class FileDetailFactory implements Factory<FileDetail> {

    public FileDetail createFromResultSet(ResultSet rs) throws SQLException {
        if (rs == null || !rs.next()) {
            return null;
        }
        FileDetailBuilder builder = new FileDetailBuilder();
        builder.setType(rs.getString(FileDetail.COL_TYPE));
        builder.setName(rs.getString(FileDetail.COL_NAME));
        builder.setDate(rs.getString(FileDetail.COL_DATE));
        builder.setSize(rs.getString(FileDetail.COL_SIZE));
        return builder.get();
    }

    @Override
    public List<FileDetail> createListFromResultSet(ResultSet rs) throws SQLException {
        return null;
    }

    public FileDetail createFromMap(Map<String, String[]> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        FileDetailBuilder builder = new FileDetailBuilder();
        builder.setType(map.get(FileDetail.COL_TYPE)[0])
                .setName(map.get(FileDetail.COL_NAME)[0])
                .setDate(map.get(FileDetail.COL_DATE)[0])
                .setSize(map.get(FileDetail.COL_SIZE)[0]);
        return builder.get();
    }

}
