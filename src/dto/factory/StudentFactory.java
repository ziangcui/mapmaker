package dto.factory;

import dto.Student;
import dto.builder.StudentBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StudentFactory implements Factory<Student> {
    @Override
    public Student createFromResultSet(ResultSet rs) throws SQLException {
        if (rs == null || !rs.next()) {
            return null;
        }
        StudentBuilder builder = new StudentBuilder();
        builder.setId(rs.getString(Student.COL_ID))
                .setFirstName(rs.getString(Student.COL_FIRST_NAME)).setLastName(rs.getString(Student.COL_LAST_NAME));
        return builder.get();
    }

    @Override
    public List<Student> createListFromResultSet(ResultSet rs) throws SQLException {
        if (rs == null || !rs.next()) {
            return null;
        }
        StudentBuilder builder;
        List<Student> students;
        students = new ArrayList<>(100);
        while (rs.next()) {
            builder = new StudentBuilder();
            builder.setId(rs.getString(Student.COL_ID))
                    .setFirstName(rs.getString(Student.COL_FIRST_NAME))
                    .setLastName(rs.getString(Student.COL_LAST_NAME));
            students.add(builder.get());
        }
        return students;
    }

    @Override
    public Student createFromMap(Map<String, String[]> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        StudentBuilder builder = new StudentBuilder();
        builder.setId(map.get(Student.COL_ID)[0])
                .setFirstName(map.get(Student.COL_FIRST_NAME)[0]).setLastName(map.get(Student.COL_LAST_NAME)[0]);
        return builder.get();
    }
}
