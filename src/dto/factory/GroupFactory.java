/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.factory;

import dto.Group;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Min Jia
 */
public class GroupFactory implements Factory<Group> {

    public Group createFromResultSet(ResultSet rs) throws SQLException {
        if (rs == null || !rs.next()) {
            return null;
        }
        dto.factory.GroupBuilder builder = new dto.factory.GroupBuilder();
        builder.setName(rs.getString(Group.COL_NAME));
        return builder.get();
    }

    @Override
    public List<Group> createListFromResultSet(ResultSet rs) throws SQLException {
        return null;
    }


    public Group createFromMap(Map<String, String[]> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        dto.factory.GroupBuilder builder = new dto.factory.GroupBuilder();
        builder.setName(map.get(Group.COL_NAME)[0]);
        return builder.get();
    }

}
