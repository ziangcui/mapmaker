package dto;

public class StudentGroupMatch {
    public static final String COL_GROUP_ID = "group_id";
    public static final String COL_STUDENT_ID = "student_id";
    public static final String COL_DATE = "date";

    private String groupId;
    private String studentId;
    private String date;

    public StudentGroupMatch(String groupId, String studentId) {
        this.groupId = groupId;
        this.studentId = studentId;
    }

    public StudentGroupMatch() {
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
