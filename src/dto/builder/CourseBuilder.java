package dto.builder;

import dto.Course;

public class CourseBuilder {

    private final Course course = new Course();

    public CourseBuilder setName(String name) {
        course.setName(name);
        return this;
    }

    public CourseBuilder setCode(String code) {
        course.setCode(code);
        return this;
    }

    public Course get() {
        return course;
    }
}
