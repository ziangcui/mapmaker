/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.builder;

import dto.FileDetail;

/**
 * @author Min Jia
 */
public class FileDetailBuilder {
    private final FileDetail fileDetail = new FileDetail();

    public FileDetailBuilder setName(String name) {
        fileDetail.setName(name);
        return this;
    }

    public FileDetailBuilder setType(String type) {
        fileDetail.setType(type);
        return this;
    }

    public FileDetailBuilder setSize(String size) {
        fileDetail.setSize(size);
        return this;
    }

    public FileDetailBuilder setDate(String date) {
        fileDetail.setDate(date);
        return this;
    }

    public FileDetail get() {
        return fileDetail;
    }

}
