/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto.builder;

import dto.File;

/**
 * @author Min Jia
 */
public class FileBuilder {

    private final File file = new File();
    private int id;

    public int getId() {
        return id;
    }

    public FileBuilder setId(String id) {
        file.setId(id);
        return this;
    }

    public File getFile() {
        return file;
    }

    public FileBuilder setFile(String b) {
        file.setFile(b);
        return this;
    }

    public File get() {
        return file;
    }
}
