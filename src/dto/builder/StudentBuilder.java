package dto.builder;

import dto.Student;

public class StudentBuilder {

    private final Student student = new Student();

    public StudentBuilder setId(String id) {
        student.setId(id);
        return this;
    }

    public StudentBuilder setFirstName(String firstName) {
        student.setFirstName(firstName);
        return this;
    }

    public StudentBuilder setLastName(String lastName) {
        student.setLastName(lastName);
        return this;
    }

    public Student get() {
        return student;
    }
}
