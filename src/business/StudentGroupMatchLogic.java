package business;

import dataaccess.StudentGroupMatchDAO;
import dataaccess.StudentGroupMatchDAOImpl;
import dto.Group;
import dto.Student;
import dto.StudentGroupMatch;

import java.util.List;

public class StudentGroupMatchLogic {

    private StudentGroupMatchDAO dao = null;

    public StudentGroupMatchLogic() {
        dao = new StudentGroupMatchDAOImpl();
    }

    public List<StudentGroupMatch> getAllMatchs() {
        return dao.getAll();
    }

    public void matchStudentGroup(Student student, Group group) {
        dao.add(student, group);
    }

    public Student getMatchStudent(Group group) {
        return dao.getMatchStudent(group);
    }

    public Group getMatchGroup(Student student) {
        return dao.getMatchGroup(student);
    }

    public void deleteStudentGroupMatch(String[] deleteMarks) {
        dao.deleteAll(deleteMarks);
    }
}
