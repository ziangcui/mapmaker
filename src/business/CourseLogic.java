package business;

import dataaccess.CourseDAOImpl;
import dataaccess.DAOInterface;
import dto.Course;
import dto.factory.DTOFactoryCreator;
import dto.factory.Factory;

import java.util.List;
import java.util.Map;

/**
 * @author Stanley Pieda
 * @course Shahriar Emami
 */
public class CourseLogic {

    private static final int COURSE_CODE_MAX_LENGTH = 45;
    private static final int COURSE_NAME_MAX_LENGTH = 45;

    private DAOInterface<Course> dao = null;
    private Factory<Course> factory = null;

    public CourseLogic() {
        dao = new CourseDAOImpl();
        factory = DTOFactoryCreator.createFactory(Course.class);
    }

    public List<Course> getAllCourses() {
        return dao.getAll();
    }

    public void addCourse(Map<String, String[]> course) throws ValidationException {
//        addCourse( factory.createFromMap(course));
        Course c = new Course(course.get(Course.COL_NAME)[0], course.get(Course.COL_CODE)[0]);
        addCourse(c);
    }

    public void addCourse(Course course) throws ValidationException {
        validateCourse(course);
        cleanCourse(course);
        dao.add(course);
    }

    public void deleteCourses(String[] codes) throws ValidationException {
        for (String code : codes) {
            deleteCourse(code);
        }
    }

    public void deleteCourse(String code) throws ValidationException {
        dao.delete(code);
    }

    private void cleanCourse(Course course) {
        if (course.getCode() != null) {
            course.setCode(course.getCode().trim());
        }
        if (course.getName() != null) {
            course.setName(course.getName().trim());
        }
    }

    private void validateCourse(Course course) throws ValidationException {
        validateString(course.getCode(), "Course Code", COURSE_CODE_MAX_LENGTH, false);
        validateString(course.getName(), "Course Name", COURSE_NAME_MAX_LENGTH, false);
    }

    private void validateString(String value, String fieldName, int maxLength, boolean isNullAllowed) throws ValidationException {
        if (value == null && isNullAllowed) {
            // null permitted, nothing to validate
        } else if (value == null && !isNullAllowed) {
            throw new ValidationException(String.format("%s cannot be null", fieldName));
        } else if (value.isEmpty()) {
            throw new ValidationException(String.format("%s cannot be empty or only whitespace", fieldName));
        } else if (value.length() > maxLength) {
            throw new ValidationException(String.format("%s cannot exceed %d characters", fieldName, maxLength));
        }
    }
}
