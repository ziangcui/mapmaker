/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import dataaccess.FileDetailDAOImpl;
import dto.FileDetail;
import dto.factory.DTOFactoryCreator;
import dto.factory.Factory;

import javax.servlet.http.Part;
import java.util.List;

/**
 * @author Min Jia
 */
public class FileDetailLogic {
    private static final int FILEDETAIL_NAME_MAX_LENGTH = 45;
    private static final int FILEDETAIL_TYPE_MAX_LENGTH = 45;
    private static final int FILEDETAIL_DATE_MAX_LENGTH = 45;
    private static final int FILEDETAIL_SIZE_MAX_LENGTH = 45;

    private FileDetailDAOImpl fileDetailDAO = null;
    private Factory<FileDetail> factory = null;

    public FileDetailLogic() {
        fileDetailDAO = new FileDetailDAOImpl();
        factory = DTOFactoryCreator.createFactory(FileDetail.class);
    }

    public List<FileDetail> getAllFileDetails() {
        return fileDetailDAO.getAll();
    }

    public void addFileDetail(Part fileDetail, String key) {
        //  cleanFileDetail(fileDetail);
        //validateFileDetail(id);
        fileDetailDAO.add(fileDetail, key);
    }

//    public void addFileDetail(Map<String, String[]> fileDetail) throws ValidationException {
//        FileDetail fd = new FileDetail(fileDetail.get(FileDetail.COL_NAME)[0],
//                fileDetail.get(FileDetail.COL_TYPE)[0], fileDetail.get(FileDetail.COL_DATE)[0], fileDetail.get(FileDetail.COL_SIZE)[0]);
//        addFileDetail(fd);
//    }

    public void deleteFileDetail(String name) {
        fileDetailDAO.delete(name);
    }

    public FileDetail getById(String id) {
        return fileDetailDAO.getById(id);
    }

    private void cleanFileDetail(FileDetail fileDetail) {
        if (fileDetail.getName() != null) {
            fileDetail.setName(fileDetail.getName().trim());
        }

        if (fileDetail.getType() != null) {
            fileDetail.setType(fileDetail.getType().trim());
        }
        if (fileDetail.getDate() != null) {
            fileDetail.setDate(fileDetail.getDate().trim());
        }
        if (fileDetail.getSize() != null) {
            fileDetail.setSize(fileDetail.getSize().trim());
        }
    }

    private void validateFileDetail(FileDetail fileDetail) throws ValidationException {
        validateString(fileDetail.getName(), "FileDetail Name", FILEDETAIL_NAME_MAX_LENGTH, false);
        validateString(fileDetail.getType(), "FileDetail Type", FILEDETAIL_TYPE_MAX_LENGTH, false);
        validateString(fileDetail.getDate(), "FileDetail Date", FILEDETAIL_DATE_MAX_LENGTH, false);
        validateString(fileDetail.getSize(), "FileDetail Size", FILEDETAIL_SIZE_MAX_LENGTH, false);
    }

    private void validateString(String value, String fieldName, int maxLength, boolean isNullAllowed) throws ValidationException {
        if (value == null && isNullAllowed) {
            // null permitted, nothing to validate
        } else if (value == null && !isNullAllowed) {
            throw new ValidationException(String.format("%s cannot be null", fieldName));
        } else if (value.isEmpty()) {
            throw new ValidationException(String.format("%s cannot be empty or only whitespace", fieldName));
        } else if (value.length() > maxLength) {
            throw new ValidationException(String.format("%s cannot exceed %d characters", fieldName, maxLength));
        }
    }

    public void deleteFileDetails(String[] names) {
        for (String name : names) {
            deleteFileDetail(name);
        }
    }

}
