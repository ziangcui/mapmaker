/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import dataaccess.GroupDAO;
import dataaccess.GroupDAOImpl;
import dto.Group;
import dto.factory.DTOFactoryCreator;
import dto.factory.Factory;

import java.util.List;
import java.util.Map;

/**
 * @author Min Jia
 */
public class GroupLogic {
    private static final int GROUP_NAME_MAX_LENGTH = 45;

    private GroupDAO dao = null;
    private Factory<Group> factory = null;

    public GroupLogic() {
        dao = new GroupDAOImpl();
        factory = DTOFactoryCreator.createFactory(Group.class);
    }

    public List<Group> getAllGroups() {
        return dao.getAll();
    }

    public void addGroup(Map<String, String[]> group) throws ValidationException {
        //addGroup( factory.createFromMap(group));
        Group g = new Group(group.get(Group.COL_NAME)[0]);
        addGroup(g);
    }

    public void addGroup(Group group) {
        cleanGroup(group);
        dao.add(group);
    }

    public void deleteGroup(String[] names) {
        for (String name : names) {
            deleteGroup(name);
        }
    }

    public void deleteGroup(String name) {
        dao.delete(name);
    }

    private void cleanGroup(Group group) {
        if (group.getName() != null) {
            group.setName(group.getName().trim());
        }
    }

    private void validateGroup(Group group) throws ValidationException {
        validateString(group.getName(), "Group Name", GROUP_NAME_MAX_LENGTH, false);
    }

    private void validateString(String value, String fieldName, int maxLength, boolean isNullAllowed) throws ValidationException {
        if (value == null && isNullAllowed) {
            // null permitted, nothing to validate
        } else if (value == null && !isNullAllowed) {
            throw new ValidationException(String.format("%s cannot be null", fieldName));
        } else if (value.isEmpty()) {
            throw new ValidationException(String.format("%s cannot be empty or only whitespace", fieldName));
        } else if (value.length() > maxLength) {
            throw new ValidationException(String.format("%s cannot exceed %d characters", fieldName, maxLength));
        }
    }

    public Group getById(String id) {
        return dao.getById(id);
    }
}
