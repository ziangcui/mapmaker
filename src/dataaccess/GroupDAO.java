/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataaccess;

import dto.Group;

import java.util.List;

/**
 * @author Min Jia
 */
public interface GroupDAO {
    List<Group> getAll();

    void add(Group group);

    void delete(String str);

    void deleteAll(String[] str);

    Group getById(String id);


}
