package dataaccess;

import dto.File;
import dto.Group;
import dto.Student;

public interface FileStudnetGroupMatchDAO {

    void add(File file, Student student, Group group);

    void delete(String str);

}
