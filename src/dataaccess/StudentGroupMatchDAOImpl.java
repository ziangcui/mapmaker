package dataaccess;

import business.GroupLogic;
import business.StudentLogic;
import dto.Group;
import dto.Student;
import dto.StudentGroupMatch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudentGroupMatchDAOImpl implements StudentGroupMatchDAO {
    private static final String GET_ALL_MATCHS = "SELECT group_id, student_id, `date` FROM studentgroupmatch ORDER BY `date`";
    private static final String INSERT_MATCHER = "INSERT INTO studentgroupmatch (`group_id`, student_id, date) VALUES (?,?,now())";
    private static final String DELETE_STUDENT_GROUP_MATCH = "DELETE FROM studentgroupmatch WHERE id = ?";
    private static final String DELETE_STUDENT_GROUP_MATCHS = "DELETE FROM studentgroupmatch WHERE (student_id) IN ";
    private static final String SELECT_MATCH_STUDENT = "SELECT group_id, student_id, `date` FROM studentgroupmatch WHERE group_id = ? ORDER BY `date` ";
    private static final String SELECT_MATCH_GROUP = "SELECT group_id, student_id, `date` FROM studentgroupmatch WHERE student_id = ? ORDER BY `date` ";

    @Override
    public void add(Student student, Group group) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_MATCHER)) {
            pstmt.setString(1, group.getId());
            pstmt.setString(2, student.getId());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Student getMatchStudent(Group group) {
        StudentLogic studentLogic = new StudentLogic();
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(SELECT_MATCH_STUDENT)) {
            pstmt.setString(1, group.getId());
            pstmt.executeQuery();
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                Student student = studentLogic.getById(rs.getString("student_id"));
                return student;
            } else return null;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<StudentGroupMatch> getAll() {
        List<StudentGroupMatch> studentGroupMatchs = Collections.emptyList();
        StudentGroupMatch studentGroupMatch;
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(GET_ALL_MATCHS);
             ResultSet rs = pstmt.executeQuery()) {
            studentGroupMatchs = new ArrayList<>(100);
            while (rs.next()) {
                studentGroupMatch = new StudentGroupMatch();
                studentGroupMatch.setGroupId(rs.getString(StudentGroupMatch.COL_GROUP_ID));
                studentGroupMatch.setStudentId((rs.getString(StudentGroupMatch.COL_STUDENT_ID)));
                studentGroupMatch.setDate((rs.getString(StudentGroupMatch.COL_DATE)));
                studentGroupMatchs.add(studentGroupMatch);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileDetailDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return studentGroupMatchs;
    }

    @Override
    public Group getMatchGroup(Student student) {
        GroupLogic groupLogic = new GroupLogic();
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(SELECT_MATCH_GROUP)) {
            pstmt.setString(1, student.getId());
            pstmt.executeQuery();
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                Group group = groupLogic.getById(rs.getString("group_id"));
                return group;
            } else return null;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void delete(String str) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(DELETE_STUDENT_GROUP_MATCH);) {
            pstmt.setString(1, str);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(StudentGroupMatch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteAll(String[] str) {
        StringBuilder query = new StringBuilder(DELETE_STUDENT_GROUP_MATCHS);
        query.append("(");
        String delimiter = "";
        for (int i = 0; i < str.length; i++) {
            query.append(delimiter).append("(?)");
            delimiter = ",";
        }
        query.append(")");
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(query.toString());) {
            for (int i = 0; i < str.length; i++) {
                pstmt.setString(i + 1, str[i]);
            }
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Student.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
