package dataaccess;

import dto.FileDetail;

import javax.servlet.http.Part;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileDetailDAOImpl implements DAOInterface<FileDetail> {
    private static final String GET_ALL_FILE_DETAILS = "SELECT id, file FROM FileDetail ORDER BY id";
    private static final String INSERT_FILE_DETAIL = "INSERT INTO FileDetail (file_id,name, type, date, size) VALUES(?, ?, ?, NOW(), ?)";
    private static final String DELETE_FILE_DETAIL = "DELETE FROM FileDetail WHERE file_id = ?";
    private static final String DELETE_FILE_DETAILS = "DELETE FROM Courses WHERE (course_num) IN ";
    private static final String UPDATE_FILE_DETAIL = "UPDATE Courses SET name = ? WHERE course_num = ?";
    private static final String GET_BY_FILE_DETAIL_ID = "SELECT name, type, date, size FROM FileDetail WHERE file_id = ?";

    @Override
    public List<FileDetail> getAll() {
        List<FileDetail> fileDetails = Collections.emptyList();
        FileDetail fileDetail;
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(GET_ALL_FILE_DETAILS);
             ResultSet rs = pstmt.executeQuery()) {
            fileDetails = new ArrayList<>(100);
            while (rs.next()) {
                fileDetail = new FileDetail();
                fileDetail.setName(rs.getString(FileDetail.COL_NAME));
                fileDetail.setType(rs.getString(FileDetail.COL_TYPE));
                fileDetail.setDate(rs.getString(FileDetail.COL_DATE));
                fileDetail.setSize(rs.getString(FileDetail.COL_SIZE));
                fileDetails.add(fileDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileDetailDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fileDetails;
    }

    @Override
    public FileDetail getById(String id) {
        FileDetail fileDetail = null;
        try {
            Connection con = DataSource.createConnection();
            PreparedStatement pstmt = con.prepareStatement(GET_BY_FILE_DETAIL_ID);
            pstmt.setString(1, id);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                fileDetail = new FileDetail();
                fileDetail.setName(rs.getString(FileDetail.COL_NAME));
                fileDetail.setType(rs.getString(FileDetail.COL_TYPE));
                fileDetail.setDate(rs.getString(FileDetail.COL_DATE));
                fileDetail.setSize(rs.getString(FileDetail.COL_SIZE));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileDetailDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fileDetail;
    }

    @Override
    public void add(FileDetail fileDetail) {

    }

    public void add(Part fileDetail, String key) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_FILE_DETAIL);) {
            pstmt.setString(1, key);
            pstmt.setString(2, fileDetail.getSubmittedFileName());
            pstmt.setString(3, fileDetail.getContentType());
            pstmt.setString(4, (String.valueOf(fileDetail.getSize())));
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(String file_id) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(DELETE_FILE_DETAIL);) {
            pstmt.setString(1, file_id);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FileDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteAll(String[] str) {
        StringBuilder query = new StringBuilder(DELETE_FILE_DETAILS);
        query.append("(");
        String delimiter = "";
        for (int i = 0; i < str.length; i++) {
            query.append(delimiter).append("(?)");
            delimiter = ",";
        }
        query.append(")");
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(query.toString());) {
            for (int i = 0; i < str.length; i++) {
                pstmt.setString(i + 1, str[i]);
            }
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FileDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
