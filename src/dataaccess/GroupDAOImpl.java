package dataaccess;

import dto.Group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GroupDAOImpl implements GroupDAO {
    private static final String GET_ALL_GROUPS = "SELECT id, name FROM `GROUP` ORDER BY id";
    private static final String INSERT_GROUP = "INSERT INTO `group`(`name`) VALUES (?)";
    private static final String DELETE_GROUP = "DELETE FROM `mapmaker`.`group` WHERE id = ?";
    private static final String DELETE_GROUPS = "DELETE FROM Courses WHERE (course_num) IN ";
    private static final String UPDATE_GROUP = "UPDATE Courses SET name = ? WHERE course_num = ?";
    private static final String GET_BY_CODE_GROUP = "SELECT id, name FROM `GROUP` WHERE id = ?";

    @Override
    public List<Group> getAll() {
        List<Group> groups = Collections.emptyList();
        Group group;
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(GET_ALL_GROUPS);
             ResultSet rs = pstmt.executeQuery();) {
            groups = new ArrayList<>(100);
            while (rs.next()) {
                group = new Group();
                group.setId(rs.getString(Group.COL_ID));
                group.setName(rs.getString(Group.COL_NAME));
                groups.add(group);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileDetailDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return groups;
    }

    @Override
    public Group getById(String id) {
        Group group = null;
        try {
            Connection con = DataSource.createConnection();
            PreparedStatement pstmt = con.prepareStatement(GET_BY_CODE_GROUP);
            pstmt.setString(1, id);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                group = new Group();
                group.setId(rs.getString(Group.COL_ID));
                group.setName(rs.getString(Group.COL_NAME));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileDetailDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return group;
    }

    @Override
    public void add(Group group) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_GROUP)) {
            pstmt.setString(1, group.getName());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(String id) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(DELETE_GROUP);) {
            pstmt.setString(1, id);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteAll(String[] str) {
        StringBuilder query = new StringBuilder(DELETE_GROUPS);
        query.append("(");
        String delimiter = "";
        for (int i = 0; i < str.length; i++) {
            query.append(delimiter).append("(?)");
            delimiter = ",";
        }
        query.append(")");
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(query.toString());) {
            for (int i = 0; i < str.length; i++) {
                pstmt.setString(i + 1, str[i]);
            }
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
