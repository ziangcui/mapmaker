/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataaccess;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Shariar
 */
public class DataSource {


    private static final DataSource DATA_SOURCE = new DataSource();

    private static javax.sql.DataSource dataSource;

    private DataSource() {
    }

    public static Connection createConnection() {

        try {
            if(dataSource ==null){
                Context initContext = new InitialContext();
                Context envContext = (Context) initContext.lookup("java:/comp/env");
                dataSource = (javax.sql.DataSource) envContext.lookup("jdbc/MapMaker");
            }
            return dataSource.getConnection();
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
