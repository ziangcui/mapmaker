package dataaccess;

import dto.File;
import dto.factory.DTOFactoryCreator;
import dto.factory.Factory;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Shawn
 */
public class FileDAOImpl implements FileDAO {

    private static final String INSERT_FILE = "insert into MapMaker.File (file) values (?)";
    private static final String GET_ALL_FILES = "select * from MapMaker.File";
    private static final String GET_FILE = "select id, file from MapMaker.File where id = ?";
    private static final String DELETE_FILE = "delete from MapMaker.File where id = ?";

    private final Factory<File> factory = DTOFactoryCreator.createFactory(File.class);

    @Override
    public List<File> getAll() {
        List<File> files = Collections.emptyList();
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(GET_ALL_FILES);
             ResultSet rs = pstmt.executeQuery()) {
            files = new ArrayList<>(100);
            while (rs.next()) {
                File file;
                file = new File();
                file.setId(rs.getString(File.COL_ID));
                file.setFile(rs.getString(File.COL_FILE));
                files.add(file);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return files;
    }

    @Override
    public int add(InputStream stream) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement statement = con.prepareStatement(INSERT_FILE, Statement.RETURN_GENERATED_KEYS)) {
            //replace the ? in the query with the given loaded path
            statement.setBlob(1, stream);
            //insert the stream to DB
            statement.executeUpdate();
            //grab the latest auto increamented key from DB
            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                return keys.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    @Override
    public File getById(String id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void update(String id, InputStream stream) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete(String id) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement statement = con.prepareStatement(DELETE_FILE)) {
            //replace the ? in the query with the given id
            statement.setString(1, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FileDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteAll(String[] ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
