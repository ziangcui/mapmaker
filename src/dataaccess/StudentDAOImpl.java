package dataaccess;

import dto.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudentDAOImpl implements StudentDAO {
    private static final String GET_ALL_STUDENTS = "SELECT id, first_name, last_name FROM Student ORDER BY id";
    private static final String INSERT_STUDENT = "INSERT INTO student (id, first_name, last_name) VALUES(?, ?, ?)";
    private static final String DELETE_STUDENT = "DELETE FROM student WHERE id = ?";
    private static final String DELETE_STUDENTS = "DELETE FROM Student WHERE (id) IN ";
    private static final String UPDATE_STUDENT = "UPDATE Courses SET name = ? WHERE course_num = ?";
    private static final String GET_BY_CODE_STUDENT = "SELECT id, first_name, last_name FROM Student WHERE id = ?";

    @Override
    public List<Student> getAll() {
        List<Student> students = Collections.emptyList();
        Student student;
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(GET_ALL_STUDENTS);
             ResultSet rs = pstmt.executeQuery()) {
            students = new ArrayList<>(100);
            while (rs.next()) {
                student = new Student();
                student.setId(rs.getString(Student.COL_ID));
                student.setFirstName(rs.getString(Student.COL_FIRST_NAME));
                student.setLastName(rs.getString(Student.COL_LAST_NAME));
                students.add(student);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileDetailDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return students;
    }

    @Override
    public Student getById(String id) {
        Student student = null;
        try {
            Connection con = DataSource.createConnection();
            PreparedStatement pstmt = con.prepareStatement(GET_BY_CODE_STUDENT);
            pstmt.setString(1, id);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                student = new Student();
                student.setId(rs.getString(Student.COL_ID));
                student.setFirstName(rs.getString(Student.COL_FIRST_NAME));
                student.setLastName(rs.getString(Student.COL_LAST_NAME));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FileDetailDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return student;
    }

    @Override
    public void add(Student student) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(INSERT_STUDENT);) {
            pstmt.setString(1, student.getId());
            pstmt.setString(2, student.getFirstName());
            pstmt.setString(3, student.getLastName());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(String id) {
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(DELETE_STUDENT);) {
            pstmt.setString(1, id);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Student.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteAll(String[] str) {
        StringBuilder query = new StringBuilder(DELETE_STUDENTS);
        query.append("(");
        String delimiter = "";
        for (int i = 0; i < str.length; i++) {
            query.append(delimiter).append("(?)");
            delimiter = ",";
        }
        query.append(")");
        try (Connection con = DataSource.createConnection();
             PreparedStatement pstmt = con.prepareStatement(query.toString());) {
            for (int i = 0; i < str.length; i++) {
                pstmt.setString(i + 1, str[i]);
            }
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Student.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
