package dataaccess;

import dto.Group;
import dto.Student;
import dto.StudentGroupMatch;

import java.util.List;

public interface StudentGroupMatchDAO {
    void add(Student student, Group group);

    void delete(String str);

    void deleteAll(String[] str);

    Group getMatchGroup(Student student);

    Student getMatchStudent(Group group);

    List<StudentGroupMatch> getAll();
}
